package com.daioware.picture.security.test;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import com.daioware.file.MyRandomAccessFile;
import com.daioware.picture.security.Steganography;

public class SteganographyTest {

	@Test
	public void test() throws IOException {
		Steganography est=new Steganography("Diego key");
    	String path,type,name,newImageName;
		path=new File("src/test/resources/").getAbsolutePath();
		type="jpg";
		newImageName="picture that contains hidden picture";
		name="wrapper";
		long longi;
		MyRandomAccessFile file=new MyRandomAccessFile("src/test/resources/picture to hide.jpg","r");
		MyRandomAccessFile guardado=new MyRandomAccessFile("src/test/resources/picture unhidden.jpg", "rw");
		guardado.setLength(0);
		longi=file.length();
		byte bytes[]=new byte[(int) longi];
		for(int i=0;i<longi;i++){
			bytes[i]=file.readByte();
		}	
		est.encodeImage(path,name,type,newImageName,bytes);
		for(byte b:est.decodeImage(path, newImageName)){
			guardado.writeByte(b);
		}
		guardado.close();
		file.close();
	}
}
